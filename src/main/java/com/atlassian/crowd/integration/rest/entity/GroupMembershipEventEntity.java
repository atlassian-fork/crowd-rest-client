/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="groupMembershipEvent")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupMembershipEventEntity extends AbstractEventEntity
{
    @XmlElement(name="group")
    private final GroupEntity group;

    @XmlElement(name="parentGroups")
    private final GroupEntityList parentGroups;

    @XmlElement(name="childGroups")
    private final GroupEntityList childGroups;

    /**
     * JAXB requires a no-arg constructor
     */
    private GroupMembershipEventEntity()
    {
        this.group = null;
        this.parentGroups = null;
        this.childGroups = null;
    }

    public GroupEntity getGroup()
    {
        return group;
    }

    public GroupEntityList getParentGroups()
    {
        return parentGroups;
    }

    public GroupEntityList getChildGroups()
    {
        return childGroups;
    }
}
