/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCrowdServiceException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.rest.entity.ErrorEntity;
import com.atlassian.crowd.integration.rest.service.util.ShutdownIgnoringMultiThreadedHttpConnectionManager;
import com.atlassian.crowd.integration.rest.util.JAXBContextCache;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.cache.HttpCacheContext;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.UnsupportedSchemeException;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.apache.http.impl.conn.DefaultSchemePortResolver;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides primitive building blocks for using a REST API.
 */
class RestExecutor
{
    private static final String INVALID_REST_SERVICE_MSG_FORMAT = "The following URL does not specify a valid Crowd User Management REST service: %s";
    private static final String EMBEDDED_CROWD_VERSION_NAME = "X-Embedded-Crowd-Version";
    private static final Logger logger = LoggerFactory.getLogger(RestExecutor.class);

    private static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 20;

    // Use ShutdownIgnoringMultiThreadedHttpConnectionManager because of FISH-411.
    private final ShutdownIgnoringMultiThreadedHttpConnectionManager connectionManager = new ShutdownIgnoringMultiThreadedHttpConnectionManager();

    private final String baseUrl;
    private final HttpHost httpHost;
    private final CredentialsProvider credsProvider;
    private final HttpClient client;

    private final JAXBContextCache jaxbContexts = new JAXBContextCache();

    /**
     * Constructs a new REST Crowd Client Executor instance.
     *
     * @param clientProperties connection parameters
     */
    RestExecutor(ClientProperties clientProperties)
    {
        try {
            final URI uri = new URI(clientProperties.getBaseURL());
            baseUrl = createBaseUrl(clientProperties.getBaseURL());
            this.httpHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());

            CacheConfig cacheConfig = CacheConfig.custom()
                    .setMaxCacheEntries(10)
                    .setMaxObjectSize(16384)
                    .setHeuristicCachingEnabled(false)
                    .setSharedCache(false)
                    .setAsynchronousWorkersMax(0)
                    .build();

            this.credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                new AuthScope(httpHost),
                new UsernamePasswordCredentials(clientProperties.getApplicationName(), clientProperties.getApplicationPassword()));

            RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
            setRequestConfig(requestConfigBuilder, clientProperties);
            RequestConfig requestConfig = requestConfigBuilder.build();

            this.client = CachingHttpClients.custom()
                    .setCacheConfig(cacheConfig)
                    .setDefaultRequestConfig(requestConfig)
                    .setConnectionManager(connectionManager)
                    .useSystemProperties()
                    .build();
        }
        catch (URISyntaxException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Create an {@link HttpRoute} with a specific port by filling
     * in the default with {@link DefaultSchemePortResolver}.
     */
    static HttpRoute routeFor(HttpHost host)
    {
        try
        {
            int port = DefaultSchemePortResolver.INSTANCE.resolve(host);

            HttpHost portedHost = new HttpHost(host.getHostName(), port, host.getSchemeName());
            return new HttpRoute(portedHost);
        }
        catch (UnsupportedSchemeException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    private void setRequestConfig(RequestConfig.Builder builder, ClientProperties clientProperties)
    {
        builder.setConnectTimeout(NumberUtils.toInt(clientProperties.getHttpTimeout(), 5000));
        builder.setSocketTimeout(NumberUtils.toInt(clientProperties.getSocketTimeout(), 10 * 60 * 1000));

        connectionManager.setMaxTotal(NumberUtils.toInt(clientProperties.getHttpMaxConnections(), DEFAULT_MAX_TOTAL_CONNECTIONS));

        HttpRoute httpRoute = routeFor(httpHost);

        // Also set per host connection limit because HttpClient has a default maximum connections per host of 2
        // (see client.getHttpConnectionManager().getParams().getMaxConnectionsPerHost())
        connectionManager.setMaxPerRoute(httpRoute,
              NumberUtils.toInt(clientProperties.getHttpMaxConnections(), connectionManager.getMaxTotal()));

        initProxyConfiguration(clientProperties, builder);
    }

    public RestExecutor(String baseUrl, HttpHost httpHost, CredentialsProvider credsProvider, HttpClient client)
    {
        this.baseUrl = baseUrl;
        this.httpHost = httpHost;
        this.credsProvider = credsProvider;
        this.client = client;
    }

    /**
     * Create an entirely new context to avoid issues where failing authentication will
     * cause preemptive authentication to no longer be sent.
     */
    private HttpCacheContext contextForThisThread()
    {
        HttpCacheContext context = HttpCacheContext.create();
        context.setCredentialsProvider(credsProvider);

        AuthCache authCache = new BasicAuthCache();

        BasicScheme basicAuth = new BasicScheme();
        authCache.put(httpHost, basicAuth);
        context.setAuthCache(authCache);

        return context;
    }

    private void initProxyConfiguration(ClientProperties clientProperties, RequestConfig.Builder builder)
    {
        if (clientProperties.getHttpProxyHost() != null)
        {
            HttpHost proxy = new HttpHost(clientProperties.getHttpProxyHost(), NumberUtils.toInt(clientProperties.getHttpProxyPort(), -1));
            builder.setProxy(proxy);

            if (clientProperties.getHttpProxyUsername() != null && clientProperties.getHttpProxyPassword() != null)
            {
                UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(clientProperties.getHttpProxyUsername(), clientProperties.getHttpProxyPassword());

                credsProvider.setCredentials(new AuthScope(proxy), credentials);
            }
        }
    }

    /**
     * Returns the "root" WebResource. This is the resource that's at the / of the crowd-rest-plugin plugin namespace.
     *
     * @param url URL to derive the base URL from
     * @return base URL
     */
    private static String createBaseUrl(String url)
    {
        final StringBuilder sb = new StringBuilder(url);
        if (url.endsWith("/"))
        {
            sb.setLength(sb.length() - 1);
        }
        sb.append("/rest/usermanagement/1");

        return sb.toString();
    }

    /**
     * Creates a get method.
     *
     * @param format request url template
     * @param args template arguments
     * @return get method with the specified url
     */
    MethodExecutor get(String format, Object... args)
    {
        return new MethodExecutor(new HttpGet(buildUrl(baseUrl, format, args)));
    }

    /**
     * Creates a delete method.
     *
     * @param format request url template
     * @param args template arguments
     * @return delete method with the specified url
     */
    MethodExecutor delete(String format, Object... args)
    {
        return new MethodExecutor(new HttpDelete(buildUrl(baseUrl, format, args)));
    }

    private static final ContentType APPLICATION_XML = ContentType.create("application/xml", Charsets.UTF_8);

    /**
     * Creates a post method with empty body.
     *
     * @param format request url template
     * @param args template arguments
     * @return post method with the specified url and body
     */
    MethodExecutor postEmpty(String format, Object... args)
    {
        final HttpPost method = new HttpPost(buildUrl(baseUrl, format, args));
        method.setEntity(new StringEntity("", APPLICATION_XML));
        return new MethodExecutor(method);
    }

    /**
     * Creates a post method.
     *
     * @param body request body
     * @param format request url template
     * @param args template arguments
     * @return post method with the specified url and body
     */
    MethodExecutor post(Object body, String format, Object... args)
    {
        final HttpPost method = new HttpPost(buildUrl(baseUrl, format, args));
        setBody(method, body);
        return new MethodExecutor(method);
    }

    /**
     * Creates a put method.
     *
     * @param body request body
     * @param format request url template
     * @param args template arguments
     * @return put method with the specified url and body
     */
    MethodExecutor put(Object body, String format, Object... args)
    {
        final HttpPut method = new HttpPut(buildUrl(baseUrl, format, args));
        setBody(method, body);
        return new MethodExecutor(method);
    }

    /**
     * Set a JAXB marshalled message body to post/put method.
     *
     * @param method method to set the body to
     * @param body object that supports JAXB marshalling
     */
    private void setBody(HttpEntityEnclosingRequestBase method, Object body)
    {
        final ByteArrayOutputStream bs = new ByteArrayOutputStream();
        try
        {
            getMarshaller(body.getClass()).marshal(body, bs);
        }
        catch (JAXBException e)
        {
            throw new DataBindingException("Cannot marshall object " + body, e);
        }
        method.setEntity(new ByteArrayEntity(bs.toByteArray(), APPLICATION_XML));
    }

    /**
     * Builds a URL based on baseUrl, format string and arguments. String arguments are encoded according to url
     * encoding rules.
     *
     * @param baseUrl beginning of the url that will not be formatted
     * @param format rest of the url that will be formatted
     * @param args arguments for the format string
     * @return URL based on baseUrl, format string and arguments
     */
    static String buildUrl(String baseUrl, String format, Object... args)
    {
        final Object[] encodedArgs = new Object[args.length];
        final int pathArgCount = pathArgumentCount(format);
        try
        {
            for (int i = 0; i < pathArgCount; ++i)
            {
                if (args[i] instanceof String)
                {
                    encodedArgs[i] = URLEncoder.encode((String) args[i], "utf-8").replace("+", "%20");
                }
                else
                {
                    encodedArgs[i] = args[i];
                }
            }
            for (int i = pathArgCount; i < args.length; ++i)
            {
                if (args[i] instanceof String)
                {
                    encodedArgs[i] = URLEncoder.encode((String) args[i], "utf-8");
                }
                else
                {
                    encodedArgs[i] = args[i];
                }
            }
            final String url = baseUrl + String.format(format, encodedArgs);

            logger.debug("Constructed " + url);

            return url;
        }
        catch (UnsupportedEncodingException e)
        {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Counts the amount of format specifiers in the path section of the format string.
     *
     * @param format format string containing zero or more format specifiers
     * @return the amount of format specifiers in the path section of the format string.
     */
    static int pathArgumentCount(String format)
    {
        int queryStart = format.indexOf('?');
        if (queryStart == -1)
        {
            queryStart = format.length();
        }

        int count = 0;
        for (int i = format.indexOf('%'); i != -1 && i < queryStart; i = format.indexOf('%', i + 1))
        {
            ++count;
        }
        return count;
    }

    void shutDown()
    {
        connectionManager.reallyShutdown();
    }

    /**
     * @param clazz class for which a marshaller is requested
     * @return a marshaller for the given class. Marshallers are not thread-safe!
     */
    private Marshaller getMarshaller(Class<?> clazz)
    {
        try
        {
            return jaxbContexts.getJAXBContext(clazz).createMarshaller();
        }
        catch (JAXBException e)
        {
            throw new DataBindingException("Cannot create marshaller for class " + clazz, e);
        }
    }

    /**
     * @param clazz class for which an unmarshaller is requested
     * @return an unmarshaller for the given class. Unmarshallers are not thread-safe!
     */
    private Unmarshaller getUnmarshaller(Class<?> clazz)
    {
        try
        {
            return jaxbContexts.getJAXBContext(clazz).createUnmarshaller();
        }
        catch (JAXBException e)
        {
            throw new DataBindingException("Cannot create unmarshaller for class " + clazz, e);
        }
    }

    /**
     * This class takes a method to perform and provides multiple ways to execute
     * it and handle the response.
     */
    class MethodExecutor
    {
        final HttpUriRequest method;
        final Set<Integer> statusCodesWithoutErrorEntity;

        protected HttpResponse response;

        /**
         *
         * @param method HTTP method to perform
         */
        MethodExecutor(HttpUriRequest method)
        {
            this(method, ImmutableSet.<Integer>of());
        }

        /**
         *
         * @param method HTTP method to perform
         * @param statusCodesWithoutErrorEntity status codes that do not return an ErrorEntity in the body
         */
        MethodExecutor(HttpUriRequest method, Set<Integer> statusCodesWithoutErrorEntity)
        {
            this.method = method;
            this.statusCodesWithoutErrorEntity = statusCodesWithoutErrorEntity;
        }

        /**
         * Builds a new MethodExecutor that does not make any attempt to parse the ErrorEntity from
         * the response body if the status code matches the provided one.
         * @param statusCode HTTP status code for which no attempt to parse the ErrorEntity in the body will be made
         * @return a new instance of MethodExecutor
         */
        public MethodExecutor ignoreErrorEntityForStatusCode(int statusCode)
        {
            final ImmutableSet<Integer> newStatusCodesWithoutErrorEntity =
                ImmutableSet.<Integer>builder().addAll(statusCodesWithoutErrorEntity).add(statusCode).build();
            return new MethodExecutor(method, newStatusCodesWithoutErrorEntity);
        }

        /**
         * Performs an HTTP request and returns a result.
         *
         * @param returnType type of the result
         * @return entity type
         * @throws com.atlassian.crowd.exception.OperationFailedException if the operation failed for unknown reason
         * @throws com.atlassian.crowd.exception.ApplicationPermissionException if the application does not have permission to perform the operation
         */
        <T> T andReceive(Class<T> returnType)
                throws ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException, CrowdRestException
        {
            method.setHeader("Accept", "application/xml");

            try
            {
                final int statusCode = executeCrowdServiceMethod(method);

                if (!isSuccess(statusCode))
                {
                    throwError(statusCode);
                    throw new OperationFailedException(response.getStatusLine().getReasonPhrase());
                }

                return getUnmarshaller(returnType).unmarshal(new StreamSource(response.getEntity().getContent()), returnType).getValue();
            }
            catch (IOException e)
            {
                throw new OperationFailedException(e);
            }
            catch (JAXBException e)
            {
                throw new OperationFailedException(e);
            }
            finally
            {
                if (response != null)
                {
                    EntityUtils.consumeQuietly(response.getEntity());
                }
            }
        }

        /**
         * Performs an HTTP request and returns true if the operation succeeded and false if HTTP error code 404 was returned.
         *
         * @return true if the operation succeeded and false if HTTP error code 404 was returned
         * @throws IllegalArgumentException if the error code is 400
         * @throws OperationFailedException if the operation failed for unknown reason
         * @throws ApplicationPermissionException if the application does not have permission to perform the operation
         */
        boolean doesExist()
                throws ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException, CrowdRestException
        {
            try
            {
                final int statusCode = executeCrowdServiceMethod(method);

                EntityUtils.consume(response.getEntity());

                if (isSuccess(statusCode))
                {
                    return true;
                }
                else if (statusCode == HttpStatus.SC_NOT_FOUND)
                {
                    return false;
                }
                else
                {
                    throwError(statusCode);
                    throw new OperationFailedException(response.getStatusLine().getReasonPhrase());
                }
            }
            catch (IOException e)
            {
                throw new OperationFailedException(e);
            }
        }

        /**
         * Performs an HTTP request and discards the result.
         *
         * @throws OperationFailedException if the operation failed for unknown reason
         * @throws ApplicationPermissionException if the application does not have permission to perform the operation
         */
        void andCheckResponse()
                throws ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException, CrowdRestException
        {
            method.setHeader("Accept", "application/xml");
            try
            {
                final int statusCode = executeCrowdServiceMethod(method);

                if (!isSuccess(statusCode))
                {
                    throwError(statusCode);
                    throw new OperationFailedException(response.getStatusLine().getReasonPhrase());
                }
            }
            catch (IOException e)
            {
                throw new OperationFailedException(e);
            }
            finally
            {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }

        /**
         * Return true if the status code is successful (2xx).
         *
         * @param statusCode HTTP status code
         * @return true if the status code is successful (2xx)
         */
        private boolean isSuccess(int statusCode)
        {
            return statusCode >= 200 && statusCode < 300;
        }

        /**
         * Executes the method on a Crowd service. An
         * <tt>OperationFailedException</tt> is thrown if the method was not executed on a valid Crowd
         * service.
         *
         * @param method HttpMethod
         * @return status code
         * @throws InvalidCrowdServiceException if the method was not executed on a valid Crowd REST service.
         */
        int executeCrowdServiceMethod(final HttpUriRequest method) throws InvalidCrowdServiceException, IOException
        {
            HttpCacheContext context = contextForThisThread();
            this.response = client.execute(method, context);

            logger.debug("Cache response for {} {} was {}", method.getMethod(), method.getURI(), context.getCacheResponseStatus());
            if (!isCrowdRestService(response))
            {
                EntityUtils.consumeQuietly(response.getEntity());
                throw new InvalidCrowdServiceException(String.format(INVALID_REST_SERVICE_MSG_FORMAT, method.getURI().toString()));
            }
            return response.getStatusLine().getStatusCode();
        }

        /**
         * Returns <tt>true</tt> if the response comes from a valid Crowd REST service, otherwise false.
         *
         * @param method HttpMethod after execution of the method
         * @return <tt>true</tt> if the response comes from a valid Crowd REST service.
         */
        private boolean isCrowdRestService(final HttpResponse method)
        {
            // a valid Crowd REST service would have the Embedded Crowd version in the response header
            return method.containsHeader(EMBEDDED_CROWD_VERSION_NAME);
        }

        /**
         * Throws exception based on HTTP status code
         *
         * @param errorCode HTTP error code
         * @throws com.atlassian.crowd.exception.ApplicationPermissionException if the error code is 403 (Forbidden)
         * @throws com.atlassian.crowd.exception.InvalidAuthenticationException if the error code is 401 (Unauthorized)
         * @throws com.atlassian.crowd.integration.rest.service.CrowdRestException if the error code is 300 or higher.
         * An ErrorEntity is parsed from the response body, unless the error code is one of
         * statusCodesWithoutErrorEntity, in which case the CrowdRestException does not contain such entity.
         * @throws OperationFailedException for all other responses
         */
        void throwError(int errorCode)
            throws ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException, CrowdRestException
        {
            try
            {
                if (errorCode == HttpStatus.SC_FORBIDDEN)
                {
                    throw new ApplicationPermissionException(getExceptionMessageFromResponse(response));
                }
                else if (errorCode == HttpStatus.SC_UNAUTHORIZED)
                {
                    throw new InvalidAuthenticationException(getExceptionMessageFromResponse(response));
                }
                else if (errorCode >= 300)
                {
                    if (statusCodesWithoutErrorEntity.contains(errorCode))
                    {
                        throw new CrowdRestException("HTTP error: " + errorCode + " " + response.getStatusLine().getReasonPhrase()
                                                     + ". Response body: " + EntityUtils.toString(response.getEntity()), null, errorCode);
                    }
                    else
                    {
                        ErrorEntity errorEntity = getUnmarshaller(ErrorEntity.class)
                                .unmarshal(new StreamSource(response.getEntity().getContent()), ErrorEntity.class).getValue();
                        throw new CrowdRestException(errorEntity.getMessage(), errorEntity, errorCode);
                    }
                }
            }
            catch (IOException e)
            {
                throw new OperationFailedException(response.getStatusLine().getReasonPhrase());
            }
            catch (DataBindingException dbe)
            {
                throw new OperationFailedException(response.getStatusLine().getReasonPhrase(), dbe);
            }
            catch (JAXBException e)
            {
                throw new OperationFailedException(response.getStatusLine().getReasonPhrase());
            }
            finally
            {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    private static final Pattern HTML_CONTENT_TYPE = Pattern.compile("text/html(\\s*;.*)?", Pattern.CASE_INSENSITIVE);

    static String getExceptionMessageFromResponse(HttpResponse method) throws IOException
    {
        Header h = method.getFirstHeader("Content-Type");

        if (h != null && HTML_CONTENT_TYPE.matcher(h.getValue()).matches())
        {
            return stripHtml(EntityUtils.toString(method.getEntity()));
        }
        else
        {
            return EntityUtils.toString(method.getEntity());
        }
    }

    private static final Pattern UP_TO_HTML_BODY = Pattern.compile(".*<body[^>]*>", Pattern.DOTALL);

    static String stripHtml(String s)
    {
        String onlyTheBody = UP_TO_HTML_BODY.matcher(s).replaceAll("");
        String withoutTags = onlyTheBody.replaceAll("<[^>]*>", "");

        return StringUtils.normalizeSpace(StringEscapeUtils.unescapeHtml4(withoutTags));
    }
}
