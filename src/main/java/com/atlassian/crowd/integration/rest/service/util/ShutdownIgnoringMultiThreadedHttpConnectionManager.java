/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.integration.rest.service.util;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class behaves exactly like {@link PoolingHttpClientConnectionManager},
 * with the exception of ignoring all {@link #shutdown()} calls.
 *
 * It can be used in cases when another component that must be supported makes calls to
 * {@link org.apache.http.conn.HttpClientConnectionManager#shutdown()}.
 */
public class ShutdownIgnoringMultiThreadedHttpConnectionManager extends PoolingHttpClientConnectionManager
{
    private static final Logger LOG = LoggerFactory.getLogger(ShutdownIgnoringMultiThreadedHttpConnectionManager.class);

    /**
     * Logs the call and returns without shutting down.
     */
    @Override
    public void shutdown()
    {
        // Log warning with stack trace and continue.
        try
        {
            throw new IllegalStateException();
        }
        catch (IllegalStateException e)
        {
            LOG.warn("Unwanted shutdown call detected and ignored", e);
        }
    }

    /**
     * Shuts down the connection manager.
     */
    public void reallyShutdown()
    {
        super.shutdown();
    }

    @Override
    protected void finalize() throws Throwable
    {
        reallyShutdown();
    }
}
