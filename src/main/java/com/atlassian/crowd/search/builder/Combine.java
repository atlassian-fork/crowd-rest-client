/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.search.builder;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;

import java.util.*;

/**
 * Builder for MultiTermRestrictions.
 *
 * For usage see <code>QueryBuilder</code>.
 */
public class Combine
{
    /**
     * Returns an <tt>OR</tt> boolean search restriction where only one or more of the search restrictions have to be
     * satisfied.
     *
     * @param restrictions search restrictions
     * @return <tt>OR</tt> boolean search restriction
     */
    public static BooleanRestriction anyOf(SearchRestriction... restrictions)
    {
        return new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, restrictions);
    }

    /**
     * Returns an <tt>AND</tt> boolean search restriction where all of the search restrictions have to be satisfied.
     *
     * @param restrictions search restrictions
     * @return <tt>AND</tt> boolean search restriction
     */
    public static BooleanRestriction allOf(SearchRestriction... restrictions)
    {
        return new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions);
    }

    /**
     * Returns an <tt>OR</tt> boolean search restriction where only one or more of the search restrictions have to be
     * satisfied.
     *
     * @param restrictions search restrictions
     * @return <tt>OR</tt> boolean search restriction
     */
    public static BooleanRestriction anyOf(Collection<SearchRestriction> restrictions)
    {
        return new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, restrictions);
    }

    /**
     * Returns an <tt>AND</tt> boolean search restriction where all of the search restrictions have to be satisfied.
     *
     * @param restrictions search restrictions
     * @return <tt>AND</tt> boolean search restriction
     */
    public static BooleanRestriction allOf(Collection<SearchRestriction> restrictions)
    {
        return new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions);
    }
}
