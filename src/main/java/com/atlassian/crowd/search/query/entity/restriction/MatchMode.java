/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.search.query.entity.restriction;

public enum MatchMode
{
    EXACTLY_MATCHES(true),
    STARTS_WITH(false),
    CONTAINS(false),
    LESS_THAN(false),
    GREATER_THAN(false),
    NULL(true);

    private final boolean exact;

    private MatchMode(boolean exact)
    {
        this.exact = exact;
    }

    /**
     * Returns true if this value represents an exact match mode. In other words,
     * this method returns true only if this match mode can never match with
     * more than one predetermined value.
     *
     * @return true if this match mode is exact
     */
    public boolean isExact()
    {
        return exact;
    }
}
