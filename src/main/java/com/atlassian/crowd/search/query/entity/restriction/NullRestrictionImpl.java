/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.search.query.entity.restriction;

/**
 * Implements a a NullRestriction interface.
 */
public class NullRestrictionImpl implements NullRestriction
{
    public static final NullRestriction INSTANCE = new NullRestrictionImpl();

    private NullRestrictionImpl()
    {
    }

    @Override
    public boolean equals(final Object o)
    {
        return o instanceof NullRestriction;
    }

    @Override
    public int hashCode()
    {
        return 1;
    }
}
