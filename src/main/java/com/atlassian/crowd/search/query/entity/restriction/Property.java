/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.search.query.entity.restriction;

/**
 * Property Values are accessible via {@link com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys},
 * {@link com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys}
 */
public interface Property<V>
{
    /**
     * Returns the name of the property.
     *
     * @return property name
     */
    String getPropertyName();

    /**
     * Returns the type of the property value.
     * 
     * @return class type of the property value
     */
    Class<V> getPropertyType();
}
