/*
 * Copyright © 2010 - 2015 Atlassian Corporation Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.crowd.search.query.entity.restriction;

import com.atlassian.crowd.embedded.api.SearchRestriction;

/**
 * Restriction on a search based on a property of type T.
 */
public interface PropertyRestriction<T> extends SearchRestriction
{
    /**
     * Returns the property to match on.
     *
     * @return Property object to match on.
     */
    Property<T> getProperty();

    /**
     * Returns the mode to match a property.
     *
     * @return match mode
     */
    MatchMode getMatchMode();

    /**
     * Returns the value to match against the property.
     *
     * @return value of type T to match against the property.
     */
    T getValue();
}
